package pkg

type INode interface {
	Clone() INode
	Print(s string)
}