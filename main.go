package main

import (
	"design-pattern-prototype/pkg"
	"fmt"
)

func main() {
	file1 := &pkg.File{Name: "File 1"}
	file2 := &pkg.File{Name: "File 2"}
	file3 := &pkg.File{Name: "File 3"}
	folder1 := &pkg.Folder{
		Childrens: []pkg.INode{file1},
		Name: "Folder 1",
	}
	folder2 := &pkg.Folder{
		Childrens: []pkg.INode{folder1, file2, file3},
		Name: "Folder 2",
	}
	fmt.Println("Printing for Folder 2")
	folder2.Print("   ")
	cloneFolder := folder2.Clone()
	fmt.Println("Printing for clone folder 2")
	cloneFolder.Print("   ")
}